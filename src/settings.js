module.exports = {
  title: 'Vue Element Admin',

  /**
   * @type {boolean} true | false
   * @description  是否显示右边侧边栏
   */
  showSettings: true,

  /**
   * @type {boolean} true | false
   * @description  是否需要标签
   */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description  是否固定头部
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description  是否在侧边栏中显示logo
   */
  sidebarLogo: false,

  /**
   * @type {boolean} true | false
   * @description 是否支持菜单拼音搜索
   * Bundle size minified 47.3kb,minified + gzipped 63kb
   */
  supportPinyinSearch: true,

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description 是否显示收集错误组件.
   * 默认在生产环境被使用
   * 如果你想在开发环境下使用，你可以
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
